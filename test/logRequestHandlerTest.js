const assert = require('chai').assert;
const sinon = require('sinon');
const helper = require('./helper');
const Configurapi = require('configurapi');

const logRequestHandler = require('../src/logRequestHandler');

describe('logRequestHandler', () => 
{
    it("Log a request properly", async () =>
    {    
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);
        
        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true};

        logRequestHandler.apply(context, [ev]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith("trace", `Request - event_name -  -  - {"payload":true}`));
    });

    it("Log a request properly - exclude the payload", async () =>
    {    
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);
        
        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true, "secret": "abcd", "password": "1234", "public": "data"};

        logRequestHandler.apply(context, [ev, ["secret", "password"]]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith("trace", `Request - event_name -  -  - {"payload":true,"secret":"*****","password":"*****","public":"data"}`));
    });

    it("Log a request properly - include the payload", async () =>
    {    
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);
        
        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = {"payload": true};

        logRequestHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith("trace", `Request - event_name -  -  - {"payload":true}`));
    });

    it("Log a request properly - undefined payload", async () =>
    {    
        let context = helper.createContext();

        let ev = sinon.mock(Configurapi.Event);
        
        ev.request = sinon.mock(Configurapi.Request);
        ev.response = sinon.mock(Configurapi.Response);
        ev.name = 'event_name'
        ev.payload = undefined;

        logRequestHandler.apply(context, [ev, true]);

        assert.isTrue(context.emit.called);
        assert.isTrue(context.emit.calledWith("trace", `Request - event_name -  -  - `));
    });
});