const redactor = require('mask-json');

module.exports = function logRequestHandler(event, excludedProperties = [])
{
    let payload = event.payload;
    let params = event.params;

    if(excludedProperties)
    {
        payload = require('mask-json')(excludedProperties, {replacement:'*****'})(event.payload);
    }
    
    this.emit("trace", `Request - ${event.name} - ${event.identity ? event.identity.id : ''} - ${params ? JSON.stringify(params) : ''} - ${payload ? JSON.stringify(payload) : ''}`);

    this.continue();
};