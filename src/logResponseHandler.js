module.exports = function logRequestHandler(event, excludedProperties = [])
{
    let payload = event.response ? event.response.body : '';
    
    if(excludedProperties)
    {
        payload = require('mask-json')(excludedProperties, {replacement:'*****'})(payload);
    }

    this.emit("trace", `Response - ${event.identity ? event.identity.id : ''} - ${event.response ? event.response.statusCode : 0} - ${payload ? JSON.stringify(payload) : ''}`);

    this.continue();
};